---
categories:
- community
layout: post
title: Framasoft su Rai Scuola
---
<a href="http://www.raiscuola.rai.it/">Rai Scuola</a> ha pubblicato <a href="http://www.raiscuola.rai.it/articoli/framasoft-piattaforma-di-risorse-open-source/43521/default.aspx">un intervento di Anna Nervo</a> - già protagonista della community Porte Aperte sul Web - in merito a <a href="https://framasoft.org/">Framasoft</a>, collettore di servizi di collaborazione e partecipazione online opensource. Nel video vengono messi in evidenza i vantaggi dell'utilizzo di Framasoft a scuola, tra cui la possibilità di adoperarlo anche senza account personali da far registrare da parte degli studenti minorenni e l'assenza di pubblicità invasive.

<!--more-->

La piattaforma Framasoft, di origine francese, è attivamente <a href="http://scuola.linux.it/framasoft-italiano">tradotta in italiano</a> da un gruppo di volontari.
