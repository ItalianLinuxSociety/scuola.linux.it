---
categories:
- software
layout: post
title: OpenDidattica.org
---

Da oggi è online <a href="https://opendidattica.org/">OpenDidattica.org</a>, raccolta di strumenti di condivisione e collaborazione online pronti all'uso, gratuitamente e senza registrazione.

<!--more-->

Il pubblico principale di questa nuova iniziativa sono ovviamente le scuole, chiuse a causa dell'emergenza sanitaria del COVID19 e costrette a condurre l'intera attività didattica usando strumenti online. Laddove molte offerte e soluzioni digitali - soprattutto promosse da soggetti commerciali - richiedono lunghe e farraginose procedure di registrazione, OpenDidattica intende essere un banco di prova agile per diverse applicazioni libere ed opensource che possono essere provate, usate, condivise, e successivamente installate su un proprio server (per conto proprio o con l'assistenza di un fornitore esterno).

Dal primo giorno sono disponibili una istanza Jitsi (per effettuare chiamate audio/video con chiunque, scambiando un semplice link), una istanza Cryptpad (suite di strumenti di collaborazione, come l'editor di testo o la lavagna su cui disegnare da PC diversi) ed una istanza Moodle (per pubblicare ed organizzare i propri contenuti), ma altre sono previste per il prossimo futuro.

Il progetto è promosso da <a href="http://www.ils.org/">Italian Linux Society</a> e si avvale dell'infrastruttura messa gentilmente a disposizione da <a href="https://assoprovider.it/">AssoProvider</a>, l'associazione italiana dei provider indipendenti.
