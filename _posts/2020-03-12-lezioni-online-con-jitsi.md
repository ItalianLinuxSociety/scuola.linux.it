---
categories:
- software
layout: post
title: Lezioni Online con Jitsi
---
Con le scuole chiuse a causa del COVID-19 gli insegnati si sono trovati a dover trovare rapidamente soluzioni per continuare l'attività didattica anche in remoto. Tra quelle più usate c'è <a href="https://jitsi.org/">Jitsi</a>, piattaforma open source che permette di fare call audio/video in modo estremamente facile, usando solo il browser, senza farraginose autenticazioni e installazioni.

<!--more-->

Per questo è stato approntato <a href="https://it.wikibooks.org/wiki/Software_libero_a_scuola/Jitsi">un tutorial</a> che fornisce qualche indicazione tecnica e qualche dritta pratica su come tenere una lezione usando questo strumento, che include anche una lista - in costante aggiornamento - delle istanze pubblicamente accessibili della piattaforma: essendo quasi tutte mantenute in modo volontario e con risorse messe a disposizione gratuitamente può capitare che qualcuna di queste non sia temporaneamente accessibile, ma grazie alla buona volontà di tante persone ogni settimana ne vengono messe online di nuove per far fronte alle innumerevoli richieste che arrivano.
