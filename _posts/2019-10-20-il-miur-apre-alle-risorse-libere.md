---
categories:
- software
layout: post
title: Il MIUR apre alle Risorse Libere
---
In una <a href="http://www.superando.it/files/2019/08/nota-miur-open-source-luglio-2019.pdf">nota ministeriale</a> pubblicata dal MIUR, il <a href="https://www.miur.gov.it/">Ministero dell'Istruzione, dell'Università e della Ricerca</a>, i dirigenti scolastici dei Centri Territoriali di Supporto vengono invitati a destinare parte dei propri fondi <i>"alla promozione dell'open source per la didattica inclusiva, per lo sviluppo di software libero e gratuito e per la formazione sull'uso e sulla sua diffusione capillare"</i>. <a href="http://www.superando.it/2019/08/30/il-ministero-apre-alle-risorse-libere-per-la-didattica-inclusiva/">Qui maggiori dettagli</a> sull'iniziativa, e le dichiarazioni di Francesco Fusillo, coordinatore del progetto <a href="https://sodilinux.itd.cnr.it/">So.Di.Linux</a>.

<!--more-->

Un segnale molto importante da parte del Ministero, che non solo riconosce così il valore del software libero (anche in termini di razionalizzazione delle risorse economiche) ma invita gli enti territoriali ad attivarsi direttamente per la sua diffusione ed il suo sostegno.
