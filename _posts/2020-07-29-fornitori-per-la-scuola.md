---
categories:
- community
layout: post
title: Fornitori per la Scuola
---
Esistono tante applicazioni libere per la scuola, che possono essere installate ed usate autonomamente su un proprio server, ma non è sempre semplice gestirle da soli né tantomeno trovare un referente tecnico che possa provvedere a installazione, hosting e manutenzione continuativa nel tempo.

<!--more-->

Per questo è stata allestita la pagina <a href="/fornitori">scuola.linux.it/fornitori</a>, destinata ad aggregare le piccole e medie imprese in grado di offrire questo genere di servizi e cui rivolgersi in caso di necessità. Per godere di tutti i vantaggi del software libero (tutela dei dati, indipendenza dai fornitori, configurabilità e personalizzazione) potendo comunque contare su un supporto professionale.

L'indice è aperto a tutte le realtà che vogliono essere elencate per farsi trovare più facilmente, dalle scuole ma non solo (enti di formazione, scuole private e analoghi).
