---
categories:
- community
layout: post
title: wiki.scuola.linux.it
---
E' stato allestito su <a href="http://wiki.scuola.linux.it/">wiki.scuola.linux.it</a> un nuovo wiki, su cui raccogliere e catalogare documentazione ed esperienze sull'utilizzo del software libero a scuola.

Attualmente è in corso la migrazione e la riorganizzazione dei contenuti sinora pubblicati sul <a href="http://wiildos.wikispaces.com/Pagina+iniziale">wiki della community Wii Libera La Lavagna</a>, chi volesse contribuire a questa attività può contattare Paolo Mauri all'indirizzo mail <a href="mailto:paolo@paolomauri.it">paolo@paolomauri.it</a>.
