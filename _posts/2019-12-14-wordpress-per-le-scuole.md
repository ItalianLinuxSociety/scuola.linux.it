---
categories:
- software
layout: post
title: Wordpress per le Scuole
---
Disponibile la prima versione stabile del nuovo tema Wordpress pensato per i siti delle scuole e realizzato seguendo le linee guida di <a href="https://designers.italia.it/">Designers Italia</a> - iniziativa dedicata agli aspetti grafici e funzionali dei siti web delle pubbliche amministrazioni. Include non solo i componenti grafici ma anche l'intera struttura dati ed i moduli Wordpress per organizzare le informazioni del proprio istituto in modo facile, coerente e aderente agli adempimenti richiesti per le PA.

<!--more-->

Il nuovo tema, implementato su mandato del <a href="https://teamdigitale.governo.it/">Team Digitale</a>, è disponibile in licenza AGPL <a href="https://github.com/italia/design-scuole-wordpress-theme">su questo repository GitHub</a> per tutti coloro che vogliono scaricarlo ed utilizzarlo autonomamente per aggiornare il sito della propria scuola. Inoltre nei prossimi mesi sarà messo a disposizione un completo servizio di hosting "chiavi in mano" per coloro che vorrebbero adottare la nuova piattaforma ma non hanno il tempo o le competenze per provvedere per conto proprio.
