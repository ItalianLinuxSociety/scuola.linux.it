---
categories:
- software
layout: post
title: Giua@School
---
È stato pubblicato il codice di Giua@School, nuovo registro elettronico opensource per la scuola.

Fortemente ispirato da <a href="http://www.lampschool.it/">LampSchool</a> - che, in questo genere, è certo il progetto più usato e popolare - e cresciuto in seno all'<a href="http://www.giua.gov.it/">Istituto Superiore Michele Giua</a> di Cagliari (da cui il progetto stesso prende il nome), è stato interamente reimplementato con moderni strumenti di sviluppo, ed integra una serie di funzioni addizionali utili in particolare con scuole divise in più sedi.

Il codice sorgente, distribuito in licenza AGPLv3, <a href="https://github.com/trinko/giua-school">è disponibile su GitHub</a>.
