---
categories:
- contenuti
layout: post
title: Risorse Didattiche per Wikipedia
---

Sul sito dell'associazione Wikimedia Italia <a href="https://www.wikimedia.it/cosa-facciamo/progetti-le-scuole/risorse-didattiche/">è disponibile una pagina</a> che riassume strumenti, tutorial e spunti destinati a docenti e studenti alle prese con la formazione a distanza, ed incentrati sull'uso delle tante risorse del mondo Wikipedia (che, ricordiamo, oltre alla ben nota enciclopedia collaborativa include <a href="https://commons.wikimedia.org/">il repository di immagini Commons</a>, <a href="https://www.openstreetmap.org/">la mappa collaborativa OpenStreetMap</a> e molto altro).
