---
categories:
- contenuti
layout: post
title: C3 Algebra 1 su Wikibooks
---
Completato il porting di Matematica C3 Algebra 1, il primo e più popolare volume della <a href="http://www.matematicamente.it/manuali-scolastici/">collana C3</a> proposta dalla community di Matematicamente, <a href="https://it.wikibooks.org/wiki/Algebra_1">su Wikibooks</a>.

<!--more-->

La pubblicazione sul wiki, componente della piattaforma Wikipedia dedicata appunto a pubblicazione ed edizione condivisa di libri, rende assai più facile da parte di chiunque modificare, correggere ed estendere l'opera, o anche prelevarne singoli estratti da usare in classe.

Il volume resta comunque disponibile anche in formato LaTeX e PDF, già formattati per la stampa cartacea.
