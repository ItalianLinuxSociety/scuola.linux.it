---
categories:
- community
layout: post
title: 'Supporto, Aziende, Formazione: Contatti in Rete'
---
Grazie ad un questionario recentemente diramato ai <a href="http://lugmap.linux.it">Linux Users Groups</a> italiani sono stati raccolti alcuni dati che condividiamo oggi su <a href="//scuola.linux.it/">scuola.linux.it</a>, certi che potranno essere molto utili a diverse scuole (e non solo).

<ul>
<li><strong><a href="/supporto">Mappa per il Supporto (tecnico e non)</a></strong>: i gruppi locali di assistenza che collaborano e vorrebbero collaborare con le scuole delle rispettive città, fornendo supporto operativo per l'allestimento di laboratori Linux ma anche interessanti esperienze da portare in classe agli studenti. Una occasione per spingersi oltre i soliti programmi ministeriali, conoscere e far conoscere strumenti e contenuti nuovi (e liberi), ed adottare inediti metodi didattici</li>
<li><strong><a href="/stage">Studenti al Lavoro</a></strong>: un indice di contatti per cercare collaborazioni in tema di formazione professionale su Linux, ambito in cui la domanda di competenze e risorse da parte del mercato è in continua crescita. Qui si trovano due elenchi, uno dedicato alle iniziative di alternanza scuola/lavoro presso cui inserire gli studenti delle scuole superiori ed uno per gli stage di laurea all'università, divisi per provincia di riferimento</li>
<li><strong><a href="/animatori-digitali">Formazione Animatori Digitali</a></strong>: dedicato principalmente ai poli formativi per animatori digitali che aspirano ad erogare una offerta formativa completa e critica, per una didattica realmente innovativa, con l'aiuto di gli esperti del settore</li>
</ul>

È possibile auto-segnalarsi per essere aggiunti alle suddette pagine, o comunque comunicare segnalazioni e commenti, all'indirizzo mail <a href="mailto:webmaster@linux.it">webmaster@linux.it</a>.

Grazie a tutti coloro che hanno partecipato all'indagine ed hanno contribuito ad aggregare queste importanti informazioni!
