---
categories:
- community
layout: post
title: Voting Machine in Lombardia
---
A seguito del referendum svolto ad ottobre 2017 in Regione Lombardia, i 23000 computer usati per il voto elettronico sono stati distribuiti in circa 1000 scuole della regione per utilizzi didattici. Tali computer sono stati installati con Linux Ubuntu, sistema operativo spesso poco conosciuto da docenti e personale amministrativo, e rischiano pertanto di non essere usati al meglio delle loro potenzialità.

<!--more-->

Per venire incontro a tale esigenza è stata approntata <a href="http://wiki.scuola.linux.it/doku.php?id=voting_machine_lombardia">una pagina sul Wiki di Scuola Linux</a> che fornisce dritte e consigli utili, per iniziare ad esplorare l'ambiente ed installare applicazioni utili in diversi contesti.

Per ulteriore supporto e richieste specifiche è a disposizione il gruppo <a href="http://www.wiildos.it/">Wii Libera la Lavagna</a>, frequentato da docenti e tecnici che possono rispondere ai quesiti o fornire assistenza tecnica via mail.
